package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WithdrawalTests {

    private BankClient bankClient;

    @BeforeEach
    public void init() {
        bankClient = new BankClient();
    }

    @Test
    public void bank_client_can_withdraw() {
        bankClient.deposit(1);
        assertDoesNotThrow(() -> bankClient.withdraw(1));
    }

    @Test
    public void when_withdraw_amount_1_account_balance_is_minus_1() {
        bankClient.setMaximalNegativeBalance(1);
        bankClient.withdraw(1);
        assertEquals( -1, bankClient.getBalance());
    }

    @Test
    public void several_withdrawals_should_accumulate_in_the_balance() {
        bankClient.deposit(2);
        bankClient.withdraw(1);
        bankClient.withdraw(1);
        assertEquals(0, bankClient.getBalance());
    }

    @Test
    public void cannot_withdraw_0() {
        assertThrows(InvalidAmountException.class, () -> bankClient.withdraw(0));
    }

    @Test
    public void cannot_withdraw_negative_amount() {
        assertThrows(InvalidAmountException.class, () -> bankClient.withdraw(-1));
    }

    @Test
    public void cannot_withdraw_when_maximal_negative_balance_is_reached() {
        bankClient.setMaximalNegativeBalance(0);
        assertThrows(MaximalNegativeBalanceException.class, () -> bankClient.withdraw(1));
    }
}
