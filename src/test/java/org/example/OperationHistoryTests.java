package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class OperationHistoryTests {

    private BankClient bankClient;

    @BeforeEach
    public void init() {
        bankClient = new BankClient();
    }

    @Test
    public void deposit_should_be_added_to_operation_history() {
        bankClient.deposit(1);
        assertEquals(bankClient.getOperations().size(), 1);
    }

    @Test
    public void withdrawal_should_be_added_to_operation_history() {
        bankClient.setMaximalNegativeBalance(1);
        bankClient.withdraw(1);
        assertEquals(bankClient.getOperations().size(), 1);
    }

    @Test
    public void deposit_should_appear_in_operation_history_with_the_correct_amount() {
        bankClient.deposit(17);
        bankClient.getLastOperation().ifPresentOrElse(operation ->
                assertEquals(operation.amount(), 17), () -> fail("Operation not found"));

    }

    @Test
    public void withdrawal_should_appear_in_operation_history_with_the_correct_amount() {
        bankClient.setMaximalNegativeBalance(23);
        bankClient.withdraw(23);
        bankClient.getLastOperation().ifPresentOrElse(operation ->
                assertEquals( -23, operation.amount()), () -> fail("Operation not found"));
    }

    @Test
    public void deposit_should_appear_in_operation_history_with_the_correct_date() {
        bankClient.deposit(1);
        bankClient.getLastOperation().ifPresentOrElse(operation -> {
            Duration duration = Duration.between(LocalDateTime.now(), operation.date());
            assertTrue(duration.getSeconds() <= 1); }, () -> fail("Operation not found"));
    }

    @Test
    public void withdrawal_should_appear_in_operation_history_with_the_correct_date() {
        bankClient.setMaximalNegativeBalance(1);
        bankClient.withdraw(1);
        bankClient.getLastOperation().ifPresentOrElse(operation -> {
            Duration duration = Duration.between(LocalDateTime.now(), operation.date());
            assertTrue(duration.getSeconds() <= 1); }, () -> fail("Operation not found"));
    }

    @Test
    public void deposit_should_appear_in_operation_history_with_the_correct_balance() {
        bankClient.deposit(1);
        bankClient.deposit(2);
        bankClient.getLastOperation().ifPresentOrElse(operation ->
                assertEquals(3, operation.balance()), () -> fail("Operation not found"));
    }

    @Test
    public void withdrawal_should_appear_in_operation_history_with_the_correct_balance() {
        bankClient.setMaximalNegativeBalance(3);
        bankClient.withdraw(1);
        bankClient.withdraw(2);
        bankClient.getLastOperation().ifPresentOrElse(operation ->
                assertEquals( -3, operation.balance()), () -> fail("Operation not found"));
    }

    @Test
    public void print_history_should_yield_one_line_with_one_deposit() {
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        bankClient.deposit(1);
        assertEquals(LocalDateTime.now().format(dateTimeFormat) + " 1 1\n", bankClient.printStatement());
    }

    @Test
    public void print_history_should_yield_one_line_with_one_withdrawal() {
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        bankClient.setMaximalNegativeBalance(1);
        bankClient.withdraw(1);
        assertEquals(LocalDateTime.now().format(dateTimeFormat) + " -1 -1\n", bankClient.printStatement());

    }

    @Test
    public void print_history_should_yield_two_lines_with_one_deposit_followed_by_a_withdrawal() {
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        bankClient.deposit(10);
        bankClient.withdraw(6);
        String today = LocalDateTime.now().format(dateTimeFormat);
        assertEquals(today + " 10 10\n" + today + " -6 4\n", bankClient.printStatement());
    }
}
