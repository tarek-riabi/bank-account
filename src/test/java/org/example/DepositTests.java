package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DepositTests {

    private BankClient bankClient;

    @BeforeEach
    public void init() {
        bankClient = new BankClient();
    }

    @Test
    public void bank_client_can_make_deposit() {
        assertDoesNotThrow(() -> bankClient.deposit(1));
    }

    @Test
    public void when_deposit_of_amount_1_account_balance_is_1() {
        bankClient.deposit(1);
        assertEquals(bankClient.getBalance(), 1);
    }

    @Test
    public void several_deposits_should_accumulate_in_the_balance() {
        bankClient.deposit(1);
        bankClient.deposit(1);
        assertEquals(bankClient.getBalance(), 2);
    }

    @Test
    public void cannot_deposit_0() {
        assertThrows(InvalidAmountException.class, () -> bankClient.deposit(0));
    }

    @Test
    public void cannot_deposit_negative_amount() {
        assertThrows(InvalidAmountException.class, () -> bankClient.deposit(-1));
    }
}
