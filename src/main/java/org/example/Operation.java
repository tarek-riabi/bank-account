package org.example;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public record Operation(OperationType operationType, Integer amount, LocalDateTime date, Integer previousBalance) {

    private static final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Override
    public Integer amount() {
        return amount * this.operationType.getSign();
    }

    @Override
    public LocalDateTime date() {
        return date;
    }

    public Integer balance() {
        return previousBalance() + amount();
    }

    @Override
    public String toString() {
        return String.format("%s %d %d", this.date().format(dateTimeFormat), this.amount(), this.balance());
    }
}
