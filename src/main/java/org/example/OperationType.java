package org.example;

public enum OperationType {
    DEPOSIT(1),
    WITHDRAWAL(-1);

    private final Integer sign;

    OperationType(Integer sign) {
        this.sign = sign;
    }

    public Integer getSign() {
        return sign;
    }
}
