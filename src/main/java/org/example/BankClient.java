package org.example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BankClient {

    private Integer maximalNegativeBalance = 0;
    private final List<Operation> operations = new ArrayList<>();

    public void deposit(int i) {
        if (i <= 0) {
            throw new InvalidAmountException(i);
        }
        addOperation(OperationType.DEPOSIT, i);
    }

    public Integer getBalance() {
        return operations.stream().mapToInt(Operation::amount).sum();
    }

    public void withdraw(int i) {
        if (i <= 0) {
            throw new InvalidAmountException(i);
        }
        if (this.maximalNegativeBalance != null && this.getBalance() - i < -this.maximalNegativeBalance) {
            throw new MaximalNegativeBalanceException();
        }
        addOperation(OperationType.WITHDRAWAL, i);
    }

    private void addOperation(OperationType operationType, Integer amount) {
        this.operations.add(new Operation(operationType, amount, LocalDateTime.now(), this.getBalance()));
    }

    public Optional<Operation> getLastOperation() {
        if (!this.getOperations().isEmpty()) {
            return Optional.of(getOperations().get(this.getOperations().size()-1));
        }
        return Optional.empty();
    }

    public void setMaximalNegativeBalance(int i) {
        this.maximalNegativeBalance = i;
    }

    public List<Operation> getOperations() {
        return this.operations;
    }

    public String printStatement() {
        final StringBuilder statements = new StringBuilder();
        getOperations().forEach(operation -> statements.append(operation.toString()).append("\n"));
        return statements.toString();
    }
}
