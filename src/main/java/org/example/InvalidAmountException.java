package org.example;

public class InvalidAmountException extends RuntimeException {

    private final Integer amount;

    public InvalidAmountException(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String getMessage() {
        return String.format("Invalid amount: %s", this.amount);
    }
}

